(use srfi-1)
(use libxml2)

(define (sax-demo)
  (define sax (sax:make-handler 
               (lambda (localname attribute-list)
                 (print "[on-start]: "
                        "localname: " localname " " 
                        "attribute-list: " attribute-list))
               (lambda (localname)
                 (print "[on-end]: "
                        "localname " localname))
               (lambda (characters)
                 (print "[on-chars]: " 
                        "characters: " characters))))
   (sax:parse-file sax #f "foo.xml")
   (sax:free-handler sax))

(define (dom-demo)
 (define (print-element-names node)
   (let loop ((n node))
     (when n
       
       ; (print "to-string: " (dom:to-string doc n))
    
       (when (dom:is-element-node? n)
         (print "element <" (dom:node-name n) ">" )
         (print "@ => " (dom:attributes n)))
 
       (when (dom:is-text-node? n)
         (print "content => " (dom:node-content n)))
       
       (print-element-names (dom:node-children n))
       (loop (dom:next-node n)))))

  (define ctx    (dom:make-parser-context))
  (define doc    (dom:read-file-with-context ctx "foo.xml" #f 0))
  (define root   (dom:root-element doc))
  (define valid? (dom:is-valid? ctx))

  (print "XML is valid?: " valid?)
  (print "root: " root)
  (print-element-names root)
  (dom:free-doc doc)
  (dom:cleanup-parser))


(define (text-reader-demo)
  (define tr (text-reader:make "foo.xml"))

  (define (helper tr)

    (when (text-reader:element-node? tr)
      (print "<"        (text-reader:name tr) ">")
      (print "@ => "    (text-reader:all-attributes tr)))
    (when (text-reader:text-node? tr)
      (print "value =>" (text-reader:value tr)))

    (if (> (text-reader:read-more tr) 0)
        (helper tr)))
  (helper tr)
  (text-reader:free tr))





