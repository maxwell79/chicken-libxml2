default: build

build:
	chicken-install -n 

install:
	chicken-install

install-test:
	chicken-install -test

uninstall:
	chicken-uninstall libxml2

test:
	cd tests && csi -s run.scm

clean:
	rm libxml2.build.sh libxml2.install.sh  libxml2.link  *.import.scm *.log *.types *.c *.o *.so ./examples/example
