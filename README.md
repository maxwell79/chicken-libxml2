# chicken-libxml


CHICKEN Scheme bindings to  [libxml2 XML parsing system](http://xmlsoft.org/) a.k.a. the libxml2 egg.

* Version:     0.4 (2018-11-12)
* API Docs:    https://wiki.call-cc.org/eggref/4/libxml2
* License:     MIT 
* Maintainer:  David Ireland (djireland79 at gmail dot com)
* Installation: chicken-install libxml2
* Dependencies: 
    * [libxml2](http://xmlsoft.org/) 2.9.7 or higher
