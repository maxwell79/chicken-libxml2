;;;                      _     _ _   __  ____  __ _     ____
;;;                     | |   (_) |__\ \/ /  \/  | |   |___ \
;;;                     | |   | | '_ \\  /| |\/| | |     __) |
;;;                     | |___| | |_) /  \| |  | | |___ / __/
;;;                     |_____|_|_.__/_/\_\_|  |_|_____|_____|
;;;
;;;                     ____  _           _ _
;;;                    | __ )(_)_ __   __| (_)_ __   __ _ ___
;;;                    |  _ \| | '_ \ / _` | | '_ \ / _` / __|
;;;                    | |_) | | | | | (_| | | | | | (_| \__ \
;;;                    |____/|_|_| |_|\__,_|_|_| |_|\__, |___/
;;;                                                 |___/
;;
;; chicken-libxml: Scheme bindings to the libxml2 library
;;
;; Copyright © 2018  David Ireland. 
;; All rights reserved. 
;;

(module libxml2
    
    ;;; Misc
    (attributes->string
     ;;; Text Reader
     text-reader:element-to-string
     text-reader:end-element-is? 
     text-reader:start-element-is?
     text-reader:end-element-node?
     text-reader:text-node?
     text-reader:element-node?
     text-reader:make
     text-reader:read-more
     text-reader:free
     text-reader:depth
     text-reader:node-type
     text-reader:empty-element?
     text-reader:move-to-attribute
     text-reader:all-attributes
     text-reader:move-to-next-attribute
     text-reader:move-to-first-attribute
     text-reader:move-to-element
     text-reader:next
     text-reader:next-sibling
     text-reader:name
     text-reader:value

     ;;; SAX
     sax:attributes->list
     sax:parse-file
     sax:parse-string
     sax:make-handler
     sax:free-handler

     ;;; DOM
     dom:is-element-node?
     dom:is-text-node?
     dom:is-attribute-node?
     dom:parse-string
     dom:parse-string-default
     dom:cleanup-parser
     dom:memory-dump
     dom:parse-file
     dom:free-doc
     dom:make-parser-context
     dom:read-file-with-context
     dom:is-valid?
     dom:free-parser-context
     dom:to-string
     dom:copy-doc
     dom:root-element
     dom:copy-node
     dom:copy-node-list
     dom:next-node
     dom:node-content
     dom:node-children
     dom:node-type
     dom:node-name
     dom:is-element-name?
     dom:get-attribute
     dom:attributes)

  (import scheme)

  (cond-expand
    (chicken-4
     (import foreign chicken scheme)
     (use extras
	  srfi-1
	  srfi-13
	  stack
	  loops))

    (chicken-5
     (import
       (chicken base)
       (chicken foreign)
       scheme
       srfi-1
       srfi-13
       stack)))
  

    (foreign-declare "#include \"libxml/tree.h\"")
    (foreign-declare "#include \"libxml/parser.h\"")
    (foreign-declare "#include \"libxml/xmlreader.h\"")
    (include "libxml2-core.scm"))
